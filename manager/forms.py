from django import forms

class NewProjectForm(forms.Form):
	name = forms.CharField(max_length=155, widget=forms.TextInput(
										attrs={
											'placeholder': 'Assign a name to your project',
											'class': 'text-input m-b-5',
										}
									))
	description = forms.CharField(max_length=140, widget=forms.TextInput(
										attrs={
											'placeholder': 'Give some details about the project',
											'class': 'text-input m-b-5'
										}
									))
	icon = forms.FileField(required=False, widget=forms.FileInput(
										attrs={
											'class': 'm-b-2'
										}
									))