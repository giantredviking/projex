from django.db import models
from django.contrib.auth.models import User

from manager.utils import get_image_path


class UserProfile(models.Model):
	user = models.OneToOneField(User)
	job_title = models.CharField(max_length=150, null=True)
	company = models.CharField(max_length=150, null=True)
	is_manager = models.BooleanField(default=False)

	def __unicode__(self):
		return self.user.username

class Project(models.Model):
	created_by = models.ForeignKey(UserProfile, null=True)
	name = models.CharField(max_length=150, null=True)
	description = models.CharField(max_length=255, null=True)
	date_created = models.DateTimeField(auto_now_add=True)
	icon = models.FileField(upload_to=get_image_path, blank=True, null=True)

	def __unicode__(self):
		return self.name