import os

def get_image_path(instance, filename):
	return os.path.join(str(instance), 'images', filename)