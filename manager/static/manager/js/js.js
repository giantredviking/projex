equalHeight = function(container){
	var currentTallest = 0,
	     currentRowStart = 0,
	     rowDivs = new Array(),
	     $el,
	     topPosition = 0;
	 $(container).each(function() {

	   $el = $(this);
	   $($el).height('auto')
	   topPostion = $el.position().top;

	   if (currentRowStart != topPostion) {
	     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
	       rowDivs[currentDiv].height(currentTallest);
	     }
	     rowDivs.length = 0; // empty the array
	     currentRowStart = topPostion;
	     currentTallest = $el.height();
	     rowDivs.push($el);
	   } else {
	     rowDivs.push($el);
	     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
	  }
	   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
	     rowDivs[currentDiv].height(currentTallest);
	   }
	 });
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function create_project(data) {
	var csrftoken = getCookie('csrftoken');

	data.append('created_by', user);
	data.append('name', $('#id_name').val());
	data.append('description', $('#id_description').val());
	data.append('icon', $('#id_icon')[0].files[0]);
	data.append('csrfmiddlewaretoken', csrftoken);

	$.ajax({
		url: "/api/v1/projects/",
		type: "POST",
		data: data,
		processData: false,
		contentType: false,
		success: function(json) {
			$('#id_project_name').val('');
			$('#id_project_description').val('');
		},
		error: function(xhr, errmsg, err) {
			console.log(xhr.status + ": " + xhr.responseText);
		}
	});
};

var ready = function() {

	$('#add-project-overlay > #add-project').css({
		'margin-top': $(window).height()/3,
	});

	$('.add-project').click(function(e) {
		e.preventDefault();
		$('#add-project-overlay').fadeIn();
	});

	$('#add-project-exit').click(function() {
		$('#add-project-overlay').fadeOut();
	});

	$('#add_project').submit(function(e) {
		e.preventDefault();
		data = new FormData();
		create_project(data);
		$('#add-project-overlay').fadeOut();
	});
};

var resize = function() {

	$('#add-project-overlay > #add-project').css({
		'margin-top': $(window).height()/3,
	});
	$(this).width() <= 760 ? $('.navbar-collapsable').hide() : $('.navbar-collapsable').show();
};

$(document).ready(ready);
$(window).resize(resize);