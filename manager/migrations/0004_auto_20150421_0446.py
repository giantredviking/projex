# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import manager.utils


class Migration(migrations.Migration):

    dependencies = [
        ('manager', '0003_auto_20150405_2113'),
    ]

    operations = [
        migrations.AlterField(
            model_name='project',
            name='icon',
            field=models.FileField(null=True, upload_to=manager.utils.get_image_path, blank=True),
        ),
    ]
