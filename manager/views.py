from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
import json

from manager.forms import NewProjectForm
from manager.models import Project, UserProfile

# Create your views here.
def dashboard(request):
	context = {}
	context['logged_in'] = True
	context['user'] = request.user.pk
	context['form'] = NewProjectForm
	return render(request, 'index.html', context)

def logout_user(request):
	logout(request)
	return redirect(reverse('home:home'))

def create_project(request):
	if request.method == 'POST':
		project_name = request.POST.get('project_name')
		project_description = request.POST.get('project_description')
		project_author = UserProfile.objects.get(user=request.user)

		project = Project.objects.create(created_by=project_author, name=project_name, description=project_description)
		project.save()

		response_data = {'result': 'success'}

		return HttpResponse(
			json.dumps(response_data),
			content_type = "application/json"
		)
	else:
		return HttpResponse(
			json.dumps({'result': 'failure'}),
			content_type = "application/json"
		)