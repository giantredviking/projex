from rest_framework import serializers

from manager.models import UserProfile, Project

class UserProfileSerializer(serializers.ModelSerializer):
	"""
	Serializer for UserProfile objects
	"""
	class Meta:
		model = UserProfile
		fields = ('user', 'job_title', 'company', 'is_manager')

class ProjectSerializer(serializers.ModelSerializer):
	"""
	Serializer for Project objects
	"""
	class Meta:
		model = Project
		fields = ('pk', 'created_by', 'name', 'description', 'date_created', 'icon')