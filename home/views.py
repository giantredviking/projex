from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.contrib import messages
from django.core.urlresolvers import reverse

from home.forms import LoginForm
from manager.models import UserProfile

def homeView(request):
	context = {}

	# if user is logged in, redirect to dashboard
	if request.user.is_authenticated():
		context['logged_in'] = True

	return render(request, 'home.html', context)

def loginView(request):
	# Initialize context dictionary
	context = {}
	# Include the Login Form
	form = LoginForm()

	# If the user is logged in already, redirect to their dashboard
	if request.user.is_authenticated():
		return redirect(reverse('manager:dashboard'))

	# Check if form was submitted
	if request.method == 'POST':
		form = LoginForm(request.POST)
		# If there are no errors in the form
		if form.is_valid():
			# Collect the data
			username = request.POST['username']
			password = request.POST['password']
			# Try to associate the username and password with a user
			try:
				user = authenticate(username=username, password=password)
				# If a user was returned, check that the user is active
				if user is not None:
					# If the user is active, log the user in
					if user.is_active:
						user_profile = UserProfile.objects.get(user=user)
						login(request, user)
						return redirect(reverse('manager:dashboard'))
					# Return an 'account disabled' message if user is inactive
					else:
						messages.add_message(request, messages.INFO, 'Account Disabled')
			# Return an 'invalid login' message	if no user found for data
			except User.DoesNotExist:
				context['user_dne'] = "Your username or password was incorrect."

	context['form'] = form
	resp = render(request, "login.html", context)
	return resp;