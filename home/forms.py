from django.forms import ModelForm
from django.contrib.auth.models import User
from django import forms

class LoginForm(forms.Form):
	username = forms.CharField(max_length=55, widget=forms.TextInput(
									attrs={
										'placeholder': 'Username or Email',
										'class': 'text-input m-b-5'
									}
								))
	password = forms.CharField(max_length=55, widget=forms.PasswordInput(
									attrs={
										'placeholder': 'Password',
										'class': 'text-input m-b-2'
									}
								))