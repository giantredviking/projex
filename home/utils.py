from django.shortcuts import redirect
from django.core.urlresolvers import reverse

def is_logged_in(request):
	if request.user.is_authenticated():
		return redirect(reverse('manager:dash'))
	else:
		pass

def set_session(request, user):
	request.session['_pru'] = user.id
	user.session_id = request.session['_pru']
	user.save()
	return redirect(reverse('manager:dash'))