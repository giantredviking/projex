from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

from rest_framework.urlpatterns import format_suffix_patterns

from api import views

urlpatterns = [
    url(r'^users/$', views.UserProfileList.as_view(), name='user-list'),
    url(r'^users/(?P<pk>[\d]+)/$', views.UserProfileDetail.as_view(), name='user-detail'),
    url(r'^projects/$', views.ProjectView.as_view(), name='project-list'),
    url(r'^projects/(?P<pk>[\d]+)/$', views.ProjectDetail.as_view(), name='project-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)

if settings.DEBUG:
	urlpatterns += static(settings.STATIC_URL, document_root = settings.STATIC_ROOT)
	urlpatterns += static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)