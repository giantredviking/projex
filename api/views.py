from django.shortcuts import render
from rest_framework import generics, mixins
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import Http404
from rest_framework.parsers import MultiPartParser

from manager.models import UserProfile, Project
from manager.serializers import UserProfileSerializer, ProjectSerializer

class UserProfileList(generics.ListCreateAPIView):
	"""
	Returns a list of all UserProfiles
	"""
	queryset = UserProfile.objects.all()
	serializer_class = UserProfileSerializer

class UserProfileDetail(mixins.RetrieveModelMixin,
                    mixins.UpdateModelMixin,
                    mixins.DestroyModelMixin,
                    generics.GenericAPIView):
    """
    Retrieve, update or delete a UserProfile instance.
    """
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)

class ProjectView(generics.ListCreateAPIView):
	"""
	Returns a list of all Projects
	"""
	queryset = Project.objects.all()
	serializer_class = ProjectSerializer
	parser_classes = (MultiPartParser,)

class ProjectDetail(mixins.RetrieveModelMixin,
                    mixins.UpdateModelMixin,
                    mixins.DestroyModelMixin,
                    generics.GenericAPIView):
	"""
	Retrieve, update or delete a Project instance
	"""
	queryset = Project.objects.all()
	serializer_class = ProjectSerializer

	def get(self, request, *args, **kwargs):
		return self.retrieve(request, *args, **kwargs)

	def put(self, request, *args, **kwargs):
		return self.update(request, *args, **kwargs)

	def delete(self, request, *args, **kwargs):
		return self.destroy(request, *args, **kwargs)
